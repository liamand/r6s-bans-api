FROM debian:12.1-slim
LABEL authors="liam"

COPY ./target/release/r6s-bans-api /bin/r6s-bans-api

CMD ["/bin/r6s-bans-api"]