-- Add up migration script here

create extension pg_trgm;

create table player (
    id bigserial primary key,
    username text unique not null,
    streamer_mode bool not null,
    wanted bool not null,
    convictions text[] not null default array[]::text[],
    ban_count bigint not null default 0
);

create table ban (
    id bigserial primary key,
    player_id bigint not null,
    banned_at date not null default current_date,
    method text not null,
    map text not null,
    constraint fk_player foreign key (player_id) references player (id)
);

create index ban_p_id_index on ban (player_id);

create or replace function update_ban_count() returns trigger as $$
begin
    update player
    set ban_count = ban_count + 1
    where id = new.player_id;

    return new;
end;
$$ language plpgsql;

create trigger increment_ban_count after insert on ban for each row execute function update_ban_count();