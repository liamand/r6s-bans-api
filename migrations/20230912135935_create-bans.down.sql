-- Add down migration script here

drop trigger increment_ban_count on ban;

drop function update_ban_count();

drop index ban_p_id_index;

drop table ban;

drop table player;

drop extension pg_trgm;