# R6S Bans API
A simple REST API that facilitates the recording and viewing of data
relating to bans done in Rainbow 6 Siege. 


## Rationale
This project exists to keep track of bans inflicted on players through 
manipulating the current team killing system. The bans which are 
executed are against those with 'convictions', generally speaking, those 
who are actively ruining the game for others by being rude, or team 
killing, etc.

# Installation

## Prerequisites
- Rustup
- Postgresql DB
- sqlx cli

## Installing (on Linux systems)
1. `git clone https://gitlab.com/liamand/r6s-bans-api`
2. `cd r6s-bans-api`
3. `echo DATABASE_URL="postgres://<user>:<pass>@<host>:<port>/<db>" >> .env`
4. `cargo install sqlx-cli`
5. `sqlx migrate run`
6. `cargo build --release`
7. `docker build -t r6s-bans-api:latest .`
8. `docker run -e DATABASE_URL="postgres://<user>:<pass>@<host>:<port>/<db>" -p <exposed_port>:8000 --name <name> -d r6-bans-api:latest`