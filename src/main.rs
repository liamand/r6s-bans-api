use std::env;
use axum::{routing::get, Router, Extension};
use sqlx::postgres::PgPoolOptions;
use tower_http::cors::{Any, CorsLayer};
use dotenvy::dotenv;

mod models;
mod api;

use api::routes::*;

#[tokio::main]
async fn main() {
    dotenv().ok();
    let d_uri= env::var("DATABASE_URL").expect("DATABASE_URL not set in .env");

    let cors = CorsLayer::new().allow_origin(Any);

    let pool = PgPoolOptions::new()
        .max_connections(5)
        .connect(&d_uri)
        .await
        .expect("Failed to connect to Postgres");

    let app = Router::new()
        .route("/", get(index))
        .route("/api/players", get(get_players).post(new_player))
        .route("/api/players/:id", get(get_player_id).patch(update_player))
        .route("/api/players/:id/bans", get(get_player_id_bans).post(new_ban))
        .layer(cors)
        .layer(Extension(pool))
        .fallback(api::handlers::not_found);

    let addr = std::net::SocketAddr::from(([0, 0, 0, 0], 8000));
    axum::Server::bind(&addr)
        .serve(app.into_make_service())
        .await
        .expect("Failed to start server");
}