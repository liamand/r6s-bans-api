use serde::{Deserialize, Serialize};
use sqlx::types::chrono::NaiveDate;

#[derive(Debug, Serialize, sqlx::FromRow)]
pub struct Ban {
    pub id: i64,
    pub player_id: i64,
    pub banned_at: NaiveDate,
    pub method: String,
    pub map: String,
}

#[derive(Debug, Deserialize)]
pub struct NewBan {
    pub banned_at: Option<NaiveDate>,
    pub method: String,
    pub map: String,
}
