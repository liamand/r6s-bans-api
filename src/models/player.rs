use serde::{Deserialize, Serialize};

#[derive(Debug, Serialize, sqlx::FromRow)]
pub struct Player {
    pub id: i64,
    pub username: String,
    pub streamer_mode: bool,
    pub wanted: bool,
    pub convictions: Vec<String>,
    pub ban_count: i64,
}

#[derive(Debug, Deserialize)]
pub struct NewPlayer {
    pub username: String,
    pub streamer_mode: bool,
    pub wanted: bool,
    pub convictions: Vec<String>,
}

#[derive(Debug, Deserialize)]
pub struct UpdatedPlayer {
    pub streamer_mode: bool,
    pub wanted: bool,
    pub convictions: Vec<String>,
}

#[derive(Debug, Deserialize)]
pub struct PlayerParams {
    pub username: Option<String>,
    pub streamer_mode: Option<bool>,
    pub wanted: Option<bool>,
}
