use axum::Json;
use crate::api::Res;

pub async fn not_found() -> Json<Res<String>> {
    Json(Res::new("Resource not found".to_string(), 404))
}