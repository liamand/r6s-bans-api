use axum::{Extension, Json};
use axum::extract::{Path, Query};
use axum::http::StatusCode;
use axum::response::{Html, IntoResponse};
use sqlx::{Error, PgPool};

use crate::api::{ErrorRes, Res};
use crate::models::ban::{Ban, NewBan};
use crate::models::player::{NewPlayer, Player, PlayerParams, UpdatedPlayer};

pub async fn index() -> Html<&'static str> {
    let html = "<h1><a href=\"/api/players\">R6S Bans API</a></h1>";
    Html(html)
}

pub async fn get_players(
    Query(params): Query<PlayerParams>,
    Extension(pool): Extension<PgPool>
) -> impl IntoResponse {
    let mut query = "select * from player where 1 = 1".to_string();

    if let Some(streamer_mode) = params.streamer_mode {
        query.push_str(format!(" and streamer_mode = {}", streamer_mode).as_str());
    }

    if let Some(username) = params.username {
        query.push_str(format!(" and username % \'{}\'", username).as_str());
    }

    if let Some(wanted) = params.wanted {
        query.push_str(format!(" and wanted = {}", wanted).as_str());
    }

    query.push_str(" order by id asc");
    let sql = query.as_str();
    let players_result = sqlx::query_as::<_, Player>(sql)
        .fetch_all(&pool)
        .await;

    return match players_result {
        Ok(players) => {
            let status = StatusCode::OK;
            let body = Json(Res::new(players, status.as_u16()));
            (status, body).into_response()
        },
        Err(_) => {
            let status = StatusCode::INTERNAL_SERVER_ERROR;
            let body = Json(Res::new(ErrorRes::new("Internal server error".to_string()), status.as_u16()));
            (status, body).into_response()
        }
    }
}

pub async fn get_player_id(
    Path(id): Path<i64>,
    Extension(pool): Extension<PgPool>
) -> impl IntoResponse {
    let player_result = sqlx::query_as::<_, Player>("select * from player where id = $1;")
        .bind(id)
        .fetch_optional(&pool)
        .await;

    return match player_result {
        Ok(player_option) => {
            if let Some(player) = player_option {
                let status = StatusCode::OK;
                let body = Json(Res::new(player, status.as_u16()));
                (status, body).into_response()
            } else {
                let status = StatusCode::NOT_FOUND;
                let body = Json(Res::new(ErrorRes::new("Player not found".to_string()), status.as_u16()));
                (status, body).into_response()
            }
        },
        Err(_) => {
            let status = StatusCode::INTERNAL_SERVER_ERROR;
            let body = Json(Res::new(ErrorRes::new("Internal server error".to_string()), status.as_u16()));
            (status, body).into_response()
        }
    }
}

pub async fn get_player_id_bans(
    Path(id): Path<i64>,
    Extension(pool): Extension<PgPool>
) -> impl IntoResponse {
    let bans_result = sqlx::query_as::<_, Ban>("select * from ban where player_id = $1;")
        .bind(id)
        .fetch_all(&pool)
        .await;

    return match bans_result {
        Ok(bans) => {
            let status = StatusCode::OK;
            let body = Json(Res::new(bans, status.as_u16()));
            (status, body).into_response()
        },
        Err(_) => {
            let status = StatusCode::INTERNAL_SERVER_ERROR;
            let body = Json(Res::new(ErrorRes::new("Internal server error".to_string()), status.as_u16()));
            (status, body).into_response()
        }
    }
}

pub async fn new_player(
    Extension(pool): Extension<PgPool>,
    Json(new_player): Json<NewPlayer>
) -> impl IntoResponse {
    let sql = "insert into player (username, streamer_mode, wanted, convictions) values ($1, $2, $3, $4) returning *";
    let player_result = sqlx::query_as::<_, Player>(sql)
        .bind(&new_player.username)
        .bind(new_player.streamer_mode)
        .bind(new_player.wanted)
        .bind(new_player.convictions)
        .fetch_one(&pool)
        .await;

    return match player_result {
        Ok(player) => {
            let status = StatusCode::CREATED;
            let body = Json(Res::new(player, status.as_u16()));
            (status, body).into_response()
        },
        Err(err) => {
            let (status, message) = match err {
                Error::Database(_) => {
                    (StatusCode::CONFLICT, format!("Player with username '{}' already exists", &new_player.username))
                },
                _ => {
                    (StatusCode::INTERNAL_SERVER_ERROR, "Internal server error".to_string())
                }
            };

            let body = Json(Res::new(ErrorRes::new(message), status.as_u16()));
            (status, body).into_response()
        }
    }
}

pub async fn update_player(
    Path(id): Path<i64>,
    Extension(pool): Extension<PgPool>,
    Json(updated_player): Json<UpdatedPlayer>
) -> impl IntoResponse {
    let sql = "update player set streamer_mode = $1, wanted = $2, convictions = $3 where id = $4 returning *;";
    let player_result = sqlx::query_as::<_, Player>(sql)
        .bind(updated_player.streamer_mode)
        .bind(updated_player.wanted)
        .bind(updated_player.convictions)
        .bind(id)
        .fetch_one(&pool)
        .await;

    return match player_result {
        Ok(player) => {
            let status = StatusCode::OK;
            let body = Json(Res::new(player, status.as_u16()));
            (status, body).into_response()
        },
        Err(_) => {
            let status = StatusCode::INTERNAL_SERVER_ERROR;
            let body = Json(Res::new(ErrorRes::new("Internal server error".to_string()), status.as_u16()));
            (status, body).into_response()
        }
    }
}

pub async fn new_ban(
    Path(id): Path<i64>,
    Extension(pool): Extension<PgPool>,
    Json(new_ban): Json<NewBan>
) -> impl IntoResponse {
    let ban_result = match new_ban.banned_at {
        Some(banned_at) => {
            let sql = "insert into ban (player_id, banned_at, method, map) values ($1, $2, $3, $4) returning *";
            sqlx::query_as::<_, Ban>(sql)
                .bind(id)
                .bind(banned_at)
                .bind(&new_ban.method)
                .bind(&new_ban.map)
                .fetch_one(&pool)
                .await
        },
        None => {
            let sql = "insert into ban (player_id, method, map) values ($1, $2, $3) returning *";
            sqlx::query_as::<_, Ban>(sql)
                .bind(id)
                .bind(&new_ban.method)
                .bind(&new_ban.map)
                .fetch_one(&pool)
                .await
        }
    };

    return match ban_result {
        Ok(ban) => {
            let status = StatusCode::CREATED;
            let body = Json(Res::new(ban, status.as_u16()));
            (status, body).into_response()
        },
        Err(err) => {
            let (status, message) = match err {
                Error::Database(_) => {
                    (StatusCode::NOT_FOUND, format!("Player with id '{}' does not exist", id))
                },
                _ => {
                    (StatusCode::INTERNAL_SERVER_ERROR, "Internal server error".to_string())
                }
            };
            let body = Json(Res::new(ErrorRes::new(message), status.as_u16()));
            (status, body).into_response()
        }
    }
}