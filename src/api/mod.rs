use serde::{Deserialize, Serialize};

pub mod handlers;
pub mod routes;

#[derive(Serialize, Debug)]
pub struct Res<T> {
    pub data: T,
    pub status: u16,
}

impl<T> Res<T> {
    fn new(data: T, status: u16) -> Self {
        Res { data, status }
    }
}

#[derive(Serialize)]
pub struct ErrorRes {
    pub message: String,
}

impl ErrorRes {
    fn new(message: String) -> Self {
        ErrorRes { message }
    }
}

